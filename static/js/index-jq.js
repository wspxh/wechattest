$(document).ready(function () {
//header
    $(".header").fadeIn(1000);
//dropdown
    $(".dropdown").mouseover(function () {
        $(this).children(".drop-layer").addClass("current");
        $(this).children("u").text("∧");
        $(this).css({background: "#fff"});
        $(this).css({borderColor: "#ccc"});
        $(".phone").css({background: "url('../images/jd2015img.png') 0 -26px no-repeat"});
    });
    $(".dropdown").mouseout(function () {
        $(this).children(".drop-layer").removeClass("current");
        $(this).children("u").text("∨");
        $(this).css({background: "#f1f1f1"});
        $(this).css({borderColor: "#f1f1f1"});
        $(".phone").css({background: "url('../images/jd2015img.png') 0 0 no-repeat"});
    });
//search
    var val = $(".search input").val();
    $(".search input").focus(function () {
        if ($(this).val() == val && $(this).css("color") == "rgb(153, 153, 153)") {
            $(this).val("");
            $(this).css({color: "#333"});
        }
    });
    $(".search input").blur(function () {
        if ($(this).val() == "") {
            $(this).val(val);
            $(this).css({color: "rgb(153,153,153)"});
        }
    });
    /*$(".search input").focus(function(){
     if($(this).val()=="卡通手机壳"&&$(this).css("color")=="rgb(153, 153, 153)"){
     $(this).val("");
     $(this).css({color:"#333"});
     }
     })
     $(".search input").blur(function(){
     if($(this).val()==""){
     $(this).css({color:"rgb(153,153,153)"});
     $(this).val("卡通手机壳");
     }
     })*/
//tab栏banner-wrap
    $(".list").mouseover(function () {
        $(".sorts-lists .drop-layer").eq($(this).index).addClass("block");
    });
//轮播图


});