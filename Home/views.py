# coding=UTF-8
from django.shortcuts import render
import hashlib
from django.http import HttpResponse, HttpResponseRedirect
from xml.etree import ElementTree
import requests
import json

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

oauth_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx7ad3eb5e1265918e&redirect_uri=http://123.56.1.17/&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect"


def index(request):
    code = request.GET["code"]

    ctot_url = r"https://api.weixin.qq.com/sns/oauth2/access_token"
    ctot_data = {"appid": "wx7ad3eb5e1265918e", "secret": "336beb0f51f8faa61a3228b1241b65ba",
                 "code": code,
                 "grant_type": "authorization_code"}
    ctot_reque = requests.get(ctot_url, params=ctot_data)
    ctot_reque.encoding = 'utf-8'
    ctot_result = json.loads(ctot_reque.text)

    ttoi_url = r"https://api.weixin.qq.com/sns/userinfo"
    ttoi_data = {"access_token": ctot_result['access_token'], "openid": ctot_result["openid"],
                 "lang": "zh_CN"}
    ttoi_reque = requests.get(ttoi_url, params=ttoi_data)
    # 需要转码来处理乱码问题
    ttoi_reque.encoding = 'utf-8'
    ttoi_result = json.loads(ttoi_reque.text)
    ttoi_result["sex"] = (ttoi_result["sex"] == 1) and "男" or "女"

    return render(request, "Index.html", context={"userinfo": ttoi_result, "oauthurl": oauth_url})


'''
check用来通过接入测试与被动回复消息
'''


@csrf_exempt
def check(request):
    if request.method == 'GET':
        params = request.GET
        token = "peixiaohan"
        args = [token, params['timestamp'], params['nonce']]
        args.sort()
        if hashlib.sha1("".join(args)).hexdigest() == params['signature']:
            if 'echostr' in params:
                return HttpResponse(params['echostr'])
        else:
            return HttpResponse("Invalid Request")
    elif request.method == 'POST':
        xml_recv = ElementTree.fromstring(request.body)
        ToUserName = xml_recv.find("ToUserName").text
        FromUserName = xml_recv.find("FromUserName").text
        Content = r'<a href="' + oauth_url + '">点击这里体验</a>'
    return render(request, 'reply.xml', context={'touser': FromUserName, 'fromuser': ToUserName, 'content': Content},
                  content_type='application/xml')


def jd(request):
    return render(request, 'jd/index.html')
